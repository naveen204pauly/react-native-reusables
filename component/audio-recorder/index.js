import React, {useState} from 'react';
import {
  View,
  TouchableOpacity,
  Image,
  StyleSheet,
  NativeModules,
} from 'react-native';
import {Icon} from '../../../assets/icon';
import NPAText from '../npa-text';
import BMAudioView from '../audio-view/audio-view';
import {strings} from '../../localization';

const BMAudioRecorder = ({
  onPressStartRecording,
  onPressPause,
  onPressFinish,
  caseId,
  uri
}) => {
  const [isRecording, setIsRecording] = useState(false);
  const [isFinished, setIsFinished] = useState(false);
  return (
    <View>
      {!isFinished && (
        <View style={styles.container}>
          {!isRecording && !isFinished && (
            <TouchableOpacity
              style={styles.startRecordView}
              onPress={() => {
                setIsRecording(true);
                NativeModules.Record.startRecording(false, caseId);
              }}>
              <View style={styles.micImageView}>
                <Image source={Icon.mic} style={styles.micIcon} />
              </View>
              <NPAText
                text={strings.startRecording}
                style={styles.startRecText}
                textColor={'#162D4E'}
              />
            </TouchableOpacity>
          )}
          {isRecording && !isFinished && (
            <View style={styles.onRecordingView}>
              <View style={styles.onRecordView}>
                <View style={styles.onRecordImageView}>
                  {/* <Image source={Icon.mic} style={styles.micIcon} /> */}
                  <View style={styles.onRecordIcon}></View>
                </View>
                <NPAText
                  text={strings.recording + ' - 00.02'}
                  style={styles.recordingText}
                  textColor={'#162D4E'}
                />
              </View>
              <TouchableOpacity style={styles.onRecordView}
                onPress={() => {
                  setIsRecording(false);
                  NativeModules.Record.stopRecording(true);
                }}>
              
                <View style={styles.puaseImageView}>
                  <Image
                    source={Icon.pause}
                    resizeMode={'contain'}
                    style={[styles.pauseIcon, {tintColor: 'white'}]}
                    resizeMode={'contain'}
                  />
                </View>
                <NPAText
                  text={strings.pause}
                  style={styles.recordingText}
                  textColor={'#162D4E'}
                />
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.onRecordView}
                onPress={() => {
                  setIsFinished(true);
                  setIsRecording(false);
                  NativeModules.Record.stopRecording(true);
                }}>
                <View style={styles.puaseImageView}>
                  {/* <Image source={Icon.mic} style={styles.micIcon} /> */}
                  <View style={styles.onPauseIcon}></View>
                </View>
                <NPAText
                  text={strings.finish}
                  style={styles.recordingText}
                  textColor={'#162D4E'}
                />
              </TouchableOpacity>
            </View>
          )}
        </View>
      )}
      {isFinished && (
        <>
          <View style={styles.audioView}>
            <View style={{flex: 1}}>
              <BMAudioView uri={uri} style={{margin: 10}} />
            </View>
            <TouchableOpacity
              onPress={() => {
                console.log('Delete Icon pressed');
              }}>
              <Image source={Icon.mic} style={styles.deleteIocn} />
            </TouchableOpacity>
          </View>
        </>
      )}
    </View>
  );
};

export default BMAudioRecorder;

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    borderRadius: 3,
    borderWidth: 1,
    borderStyle: 'dashed',
    borderColor: '#16285264',
    marginHorizontal: 10,
    backgroundColor: '#FFFFFFA2',
  },
  startRecordView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
  },
  micImageView: {
    height: 35,
    width: 35,
    borderColor: '#D0D4DC',
    borderWidth: 1,
    elevation: 3,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 10,
    backgroundColor: 'white',
  },
  micIcon: {
    height: 20,
    width: 15,
  },
  startRecText: {
    textDecorationLine: 'underline',
    fontSize: 14,
  },
  onRecordingView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 10,
    marginHorizontal: 10,
    alignItems: 'center',
  },
  onRecordView: {
    alignItems: 'center',
    flexDirection: 'row',
    marginVertical: 10,
  },
  onRecordImageView: {
    height: 20,
    width: 20,
    borderColor: '#EB584B',
    borderWidth: 1,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 5,
    backgroundColor: '#EB584B4D',
  },
  onRecordIcon: {
    height: 10,
    width: 10,
    borderRadius: 50,
    backgroundColor: '#EB584B',
  },
  recordingText: {
    fontSize: 14,
  },
  puaseImageView: {
    height: 30,
    width: 30,
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 5,
    backgroundColor: '#162852',
  },
  onPauseIcon: {
    height: 12,
    width: 12,
    backgroundColor: 'white',
  },
  pauseIcon: {
    height: 12,
    width: 10,
    // backgroundColor: 'white',
  },
  audioView: {
    flex:1,

    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 3,
    borderWidth: 1,
    borderStyle: 'dashed',
    borderColor: '#16285264',
    marginHorizontal: 10,
    backgroundColor: '#FFFFFFA2',
  },
  deleteIocn: {
    height: 15,
    width: 12,
    tintColor: '#162852',
    marginEnd:10
  },
});
