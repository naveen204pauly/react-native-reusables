import React, {useEffect, useState} from 'react';
import {View, StyleSheet, Image, TouchableOpacity} from 'react-native';
import NPAText from '../npa-text';
import {Icon} from '../../../assets/icon';
import {useIsFocused, useTheme} from '@react-navigation/native';
import Slider from '@react-native-community/slider';
import moment from 'moment';
import {
  prepare,
  onResume,
  onPause,
  getDuration,
  setCurrentTime,
  onStop,
  getTotalDuration,
  onPlay,
  getTDuration,
} from './soundHandler';
import Sound from 'react-native-sound';

import {getFile} from '../../pages/branchManager/customerInfo/index.service';
import {MaterialIndicator, BallIndicator} from 'react-native-indicators';

const BMAudioView = ({style, docId, uri}) => {
  const [max, setMax] = useState();
  const [duration, setDuration] = useState(0);
  const [startPlaying, setStartPlaying] = useState(false);
  const [completed, setCompleted] = useState(true);
  const [intervalId, setIntervalID] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [url, setUrl] = useState(null);

  const {colors} = useTheme();

  const isFocussed = useIsFocused();

  useEffect(() => {
    return () => {
      setStartPlaying(false);
      if (url !== null && url !== undefined) onStop();
      if (intervalId !== null) clearInterval(intervalId);
    };
  }, []);

  useEffect(() => {
    if (!isFocussed) setStartPlaying(false);
  }, [isFocussed]);
  useEffect(() => {
    if (uri !== null && uri !== undefined) {
      setUrl(uri);
      prepare(uri, onStarted);
    }
  }, [uri]);

  useEffect(() => {
    console.log('Docid', docId);
    if (docId !== null) {
      setIsLoading(true);
      getFile(docId)
        .then((res) => {
          console.log('FilRes', res.data);
          // getTotalDuration(res.data.url, onStarted);

          prepare(res.data.url,(duration) => {
            // setMax(duration);
            setIsLoading(false);
        
            if (duration <= 0) {
              setMax(getTDuration());
            } else {
              setMax(getTDuration());
            }
        
            onStop();
          });
          setUrl(res.data.url);
        })
        .catch((err) => {
          console.log('FilRes', err);
        });
    }
  }, [docId]);

  useEffect(() => {
    console.log('FOcuses', isFocussed);
    if (!isFocussed && url !== null) onStop();
  }, [url, isFocussed]);

  const onStarted = (duration) => {
    // setMax(duration);
    setIsLoading(true);

    if (duration <= 0) {
      setMax(getTDuration());
    } else {
      setMax(getTDuration());
    }

    onStop();
  };

  const onCompleted = () => {
    clearInterval(intervalId);
    setDuration(0);
    setStartPlaying(false)

  };

  const playAudio = () => {
    if (intervalId !== null) {
      clearInterval(intervalId);
    }
    setIsLoading(true);
    onPlay(url, onCompleted, duration, () => {
      setIsLoading(false);
      const id = setInterval(() => {
        if (getTDuration() <= 0) {
          setMax(0);
        } else {
          setMax(getTDuration());
        }
        // setMax(getTDuration())
        // if(startPlaying)
        getDuration((sec) => {
          console.log(sec, parseInt(sec) === parseInt(max), max);
          if (sec) {
            // eslint-disable-next-line radix
            setDuration(parseInt(sec) === parseInt(max) ? 0 : parseInt(sec));
            if (parseInt(sec) === parseInt(max)) {
              clearInterval(id);
              setStartPlaying(false)
            }
          }
        });
      }, 1000);
      setIntervalID(id);
      setStartPlaying(true);
      setCompleted(false);
    });
  };

  const pauseAudio = () => {
    clearInterval(intervalId);
    setStartPlaying(false);
    onPause();
  };

  const getTime = (value) => {
    let start = moment('0:00', 'm:ss');
    return start.add(value, 'second').format('m:ss');
  };
  return (
    <View style={[styles.container, style]}>
      {/* <View style={styles.innerView}>
        <Image source={Icon.play} style={{width: 8, height: 8}}/>
        <View style={style.audioPlayer}>

        </View>
      </View> */}
      {url === null || url === undefined || isLoading ? (
        <View style={styles.row}>
          {/* <MaterialIndicator color={'#000000'} /> */}
          <BallIndicator color="white" animationDuration={800} />
        </View>
      ) : (
        <View style={[styles.audioHeader]}>
          <TouchableOpacity
            style={styles.playImageContainer}
            onPress={url && startPlaying ? pauseAudio : playAudio}>
            <Image
              style={styles.playImage}
              resizeMode={'contain'}
              source={url && startPlaying ? Icon.pause : Icon.play}
            />
          </TouchableOpacity>
          <View style={{flex: 1, marginTop: 2}}>
            <Slider
              value={duration}
              minimumValue={0}
              maximumValue={max}
              thumbImage={Icon.slider}
              style={{
                flex: 1,
              }}
              onSlidingComplete={(value) => {
                setCurrentTime(value);
                setDuration(value);
              }}
              maximumTrackTintColor={'#ffffff'}
              minimumTrackTintColor={'#162852'}
            />
            <NPAText
              text={getTime(duration) + ' / ' + getTime(max)}
              style={{fontSize: 11, marginStart: 10, marginTop: 3}}
              textColor={'#4a4a4a'}
            />
          </View>
        </View>
      )}
    </View>
  );
};

export default BMAudioView;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#E4E5EA',
    borderColor: '#D0D4DC',
    borderWidth: 1,
    borderRadius: 3,
    padding: 6,
  },
  innerView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  title: {
    fontSize: 15,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  line: {
    width: 50,
    height: 2,
    backgroundColor: '#162852',
    marginTop: 3,
  },
  audioHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },

  playImageContainer: {
    alignSelf: 'flex-start',
    alignItems: 'center',
    justifyContent: 'center',
  },
  playImage: {
    width: 20,
    height: 20,
    tintColor: '#162852',
  },
});
