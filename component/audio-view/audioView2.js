import React, {useEffect, useState} from 'react';
import {View, StyleSheet, Image, TouchableOpacity} from 'react-native';
import NPAText from '../npa-text';
import {Icon} from '../../../assets/icon';
import {useIsFocused, useTheme} from '@react-navigation/native';
import Slider from '@react-native-community/slider';
import moment from 'moment';
import {MaterialIndicator,BallIndicator} from 'react-native-indicators';

const AudioView = ({
  max = 0,
  startPlaying = false,
  duration = 0,
  setCurrentTime,
  pauseAudio,
  playAudio,
  style,
  url,
  isLoading
}) => {
  const getTime = (value) => {
    let start = moment('0:00', 'm:ss');
    return start.add(value, 'second').format('m:ss');
  };

  return (
    <View style={[styles.container, style]}>
      {/* <View style={styles.innerView}>
        <Image source={Icon.play} style={{width: 8, height: 8}}/>
        <View style={style.audioPlayer}>

        </View>
      </View> */}
      {url === null || url === undefined || isLoading ? (
        <View style={styles.row}>
            {/* <MaterialIndicator color={'#000000'} /> */}
            <BallIndicator color='white' animationDuration={800} />

            
        </View>
      ) : (
        <View style={[styles.audioHeader]}>
          <TouchableOpacity
            style={styles.playImageContainer}
            onPress={startPlaying ? pauseAudio : playAudio}>
            <Image
              style={styles.playImage}
              resizeMode={'contain'}
              source={startPlaying ? Icon.pause : Icon.play}
            />
          </TouchableOpacity>
          <View style={{flex: 1, marginTop: 2}}>
            <Slider
              value={duration}
              minimumValue={0}
              maximumValue={max}
              thumbImage={Icon.slider}
              style={{
                flex: 1,
              }}
              onSlidingComplete={(value) => {
                setCurrentTime(value);
              }}
              maximumTrackTintColor={'#ffffff'}
              minimumTrackTintColor={'#162852'}
            />
            <NPAText
              text={getTime(duration) + ' / ' + getTime(max)}
              style={{fontSize: 11, marginStart: 10, marginTop: 3}}
              textColor={'#4a4a4a'}
            />
          </View>
        </View>
      )}
    </View>
  );
};

export default AudioView;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#E4E5EA',
    borderColor: '#D0D4DC',
    borderWidth: 1,
    borderRadius: 3,
    padding: 6,
  },
  row:{
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center',
  },
  innerView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  title: {
    fontSize: 15,
  },
  line: {
    width: 50,
    height: 2,
    backgroundColor: '#162852',
    marginTop: 3,
  },
  audioHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },

  playImageContainer: {
    alignSelf: 'flex-start',
    alignItems: 'center',
    justifyContent: 'center',
  },
  playImage: {
    width: 20,
    height: 20,
    tintColor: '#162852',
  },
});
