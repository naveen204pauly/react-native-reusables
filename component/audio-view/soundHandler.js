import Sound from 'react-native-sound';

var whoosh;

const prepare = (url, onStarted) => {
  console.log('Url', url);
  whoosh = new Sound(url, Sound.MAIN_BUNDLE, (error) => {
    if (error) {
      console.log('failed to load the sound', error);
      return;
    }
    console.log(
      'duration in seconds: ' +
        whoosh.getDuration() +
        'number of channels: sdss' +
        whoosh.getNumberOfChannels(),
    );
    onStarted(whoosh.getDuration());
  });
};

const onStop = () => {
  console.log('Stop');
  if (whoosh !== undefined) {
    whoosh.pause();
    whoosh.stop(() => {
      // Note: If you want to play a sound after stopping and rewinding it,
      // it is important to call play() in a callback.
      console.log('Stopped');
    });
    whoosh.release();
  }
};

const onPlay = (url,onCompleted,duration=0,onStarted)=> {
  console.log("onPlay",url);
  whoosh = new Sound(url, Sound.MAIN_BUNDLE, (error) => {
    if (error) {
      console.log('failed to load the sound', error);
      return;
    }
    console.log(
      'duration in seconds: ' +
        whoosh.getDuration() +
        'number of channels: sdss' +
        whoosh.getNumberOfChannels(),
    );
    onStarted()
    setCurrentTime(duration)
    whoosh.play((success) => {
      if (success) {
        console.log('successfully finished playing');
        onCompleted();
      } else {
        console.log('playback failed due to audio decoding errors');
      }
    });
  });
}
const onPause = () => {
  whoosh.pause();
};
const onResume = (onCompleted) => {
  whoosh.play((success) => {
    if (success) {
      console.log('successfully finished playing');
      onCompleted();
    } else {
      console.log('playback failed due to audio decoding errors');
    }
  });
};

const getTotalDuration = (url,onStarted) => {
  // console.log('D', whoosh.getDuration());
  whoosh = new Sound(url, Sound.MAIN_BUNDLE, (error) => {
    if (error) {
      console.log('failed to load the sound', error);
      return;
    }
    console.log(
      'duration in seconds: ' +
        whoosh.getDuration() +
        'number of channels: sdss' +
        whoosh.getNumberOfChannels(),
    );
    onStarted(whoosh.getDuration());

    onStop()

  });
};

const getDuration = async (sendSeconds) => {
  whoosh.getCurrentTime((seconds) => sendSeconds(seconds));
};

const getTDuration = () => {
  return whoosh.getDuration();
};

const setCurrentTime = (time) => {
  whoosh.setCurrentTime(time);
};

export {
  prepare,
  onStop,
  getDuration,
  getTotalDuration,
  onPause,
  onResume,
  setCurrentTime,
  onPlay,
  getTDuration
};
