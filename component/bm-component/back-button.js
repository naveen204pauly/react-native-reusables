import React, { useState } from 'react';
import { Image, StyleSheet, TouchableOpacity, View } from 'react-native';
import {CommonActions, useTheme} from '@react-navigation/native';
import { Icon } from 'assets/icon';

const BackButton = ({
  style,
  textColor,
  navigation,
  srcImage = Icon.bmBack,
}) => {
  const {font} = useTheme();

    return (
        <TouchableOpacity
            style={[styles.container]}
            onPress={() => {
                navigation.goBack()
            }}>
            <Image
                style={styles.imageStyle}
                resizeMode={'contain'}
                source={srcImage} />
        </TouchableOpacity >
    );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    // borderBottomWidth: 1,
    borderColor: '#D2D4E0',
    width: '80%',
    marginEnd: 10,
  },
  imageStyle: {
    width: 10, 
    height: 15, 
    margin: 15, 
    tintColor: 'white'
  },
});

export default BackButton;
