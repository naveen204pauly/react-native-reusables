import React from 'react';
import {View, StyleSheet} from 'react-native';
import NPAText from '../npa-text';

const BMTitle = ({title, textColor='#3B3B3B',style, titleStyle}) => {
  const titleStyles = {
    ...titleStyle
  }
  return (
    <View style={[styles.container,style]}>
      <NPAText text={title} medium textColor={textColor} style={titleStyle}/>
      <View style={styles.line} />
    </View>
  );
};

export default BMTitle;

const styles = StyleSheet.create({
  container: {

  },
  line: {
    width: 50,
    height: 2,
    backgroundColor: '#FFB8B2',
    marginTop: 5,
  },
});
