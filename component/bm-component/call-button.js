import React from 'react';
import {TouchableOpacity, Image, StyleSheet} from 'react-native';
import {Icon} from '../../../assets/icon';

const BMCallButton =({onPressCall, style, image=Icon.bmCall, imageStyle}) => {
    return(
        <TouchableOpacity style={[styles.callIconView, style]} onPress={onPressCall}>
            <Image source={image} style={[{ height: 12, width: 12, tintColor: '#162D4E' },imageStyle]} />
        </TouchableOpacity>
    )
}

export default BMCallButton;

const styles = StyleSheet.create({
    callIconView: {
        height: 25,
        width: 25,
        backgroundColor: '#FFB8B2',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 50,
    },
})

