import React from 'react';
import {View, StyleSheet, Text, TouchableOpacity} from 'react-native';
import NPAText from '../npa-text';

const DoubleButtons = ({
  button1Text,
  button2Text,
  setSelected,
  selected = 0,
  containerStyle
}) => {
  return (
    <View style={[styles.container,containerStyle]}>
      <View>
        <TouchableOpacity
          onPress={() => {
              console.log("Press");
              setSelected(0)
          }}
          style={selected === 0 ? styles.selectedFirstItem : styles.itemFirst}>
          <NPAText
            text={button1Text}
            textColor={selected === 0 ? '#ffffff' : '#162D4E'}
          />
        </TouchableOpacity>
        {selected === 0 && <View style={styles.triangle} />}
      </View>
      {/* <View>
        <TouchableOpacity
          onPress={() => {
              console.log("Press");
              setSelected(1)
          }}
          style={selected === 1 ? styles.selectedLastItem : styles.itemLast}>
          <NPAText
            text={button2Text}
            textColor={selected === 1 ? '#ffffff' : '#162D4E'}
          />
        </TouchableOpacity>
        {selected === 1 && <View style={styles.triangle} />}
      </View> */}
    </View>
  );
};

export default DoubleButtons;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  itemFirst: {
    backgroundColor: '#ffffff',
    paddingHorizontal: 20,
    // paddingVertical: 8,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 10,
    borderColor: '#7D899C',
    borderWidth: 1,
    borderTopLeftRadius: 3,
    borderBottomLeftRadius: 3,
    
  },
  itemLast: {
    backgroundColor: '#ffffff',
    paddingHorizontal: 20,
    // paddingVertical: 8,
    marginBottom: 10,
    borderColor: '#7D899C',
    borderWidth: 1,
    borderTopRightRadius: 3,
    borderBottomRightRadius: 3,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
  item: {
    backgroundColor: '#ffffff',
    paddingHorizontal: 20,
    // paddingVertical: 8,
    marginBottom: 10,
    borderColor: '#7D899C',
    borderWidth: 1,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
  selectedItem: {
    backgroundColor: '#162D4E',
    paddingHorizontal: 20,
    // paddingVertical: 8,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
  selectedFirstItem: {
    backgroundColor: '#162D4E',
    paddingHorizontal: 20,
    // paddingVertical: 8,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    borderTopLeftRadius: 3,
    borderBottomLeftRadius: 3
  },
  selectedLastItem: {
    backgroundColor: '#162D4E',
    paddingHorizontal: 20,
    // paddingVertical: 8,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    borderTopRightRadius: 3,
    borderBottomRightRadius: 3
  },
  text: {
    color: '#162D4E',
  },
  selectedText: {
    color: '#ffffff',
  },
  triangle: {
    width: 0,
    height: 0,
    backgroundColor: 'transparent',
    borderStyle: 'solid',
    borderLeftWidth: 5,
    borderRightWidth: 5,
    borderBottomWidth: 10,
    borderLeftColor: 'transparent',
    borderRightColor: 'transparent',
    borderBottomColor: '#162D4E',
    transform: [{rotate: '180deg'}],
    alignSelf: 'center',
  },
});
