import React, { useState } from 'react';
import { Image, StyleSheet, TouchableOpacity, View } from 'react-native';
import {CommonActions, useTheme} from '@react-navigation/native';
import { Icon } from 'assets/icon';

const LocationButton = ({
  style,
  navigation,
  srcImage = Icon.bmLocationPopUp,
  onPressLocation
}) => {
  const {font} = useTheme();

    return (
        <TouchableOpacity onPress={onPressLocation} style={styles.floatingButtonView}>
          <Image source={srcImage} style={styles.floatingButtonIcon}/>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    floatingButtonView: {
        width: 45,
        height: 45,
        borderRadius: 50,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 5, 
        position: 'absolute',
        bottom: 40,
        end: 20,
    },
    floatingButtonIcon: { width: 15, height: 18 }
});

export default LocationButton;
