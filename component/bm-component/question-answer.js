import { useTheme } from '@react-navigation/native';
import React from 'react';
import {View, StyleSheet, Image} from 'react-native';
import NPAText from '../npa-text';
import { Icon } from '../../../assets/icon';

const BMQuestionAnswer = ({style, question, answer}) => {
    const { colors } = useTheme();
  return (
    <View style={styles.container}>
    <NPAText text={question} textColor={colors.lightText} style={styles.question}/>
    <NPAText text={answer} textColor={colors.darkText} style={styles.answer} medium />
    </View>
  );
};

export default BMQuestionAnswer;

const styles = StyleSheet.create({
  container: {
      flex: 1,
      margin: 10
  },
  question: {
    fontSize: 12,
  },
  answer: {
    fontSize: 14,
    marginTop: 5
  },
});