import React from 'react';
import { View, StyleSheet, TouchableOpacity, Image } from 'react-native';
import NPAText from '../npa-text';
import { Icon } from '../../../assets/icon';

const BMSelectButton = ({ onPressSelect, style }) => {
    return (
        <TouchableOpacity style={[styles.selectIconView, style]} onPress={onPressSelect}>
            <Image source={Icon.selected} style={{ height: 12, width: 12, tintColor: '#162852' }} />
        </TouchableOpacity>
    );
};

export default BMSelectButton;

const styles = StyleSheet.create({
    container: {

    },
    selectIconView: {
        height: 25,
        width: 25,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 50,
        backgroundColor: '#FFB8B2'
    },
});
