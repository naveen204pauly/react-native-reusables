import {useTheme} from '@react-navigation/native';
import React from 'react';
import {View, StyleSheet} from 'react-native';
import NPAText from '../npa-text';

const TextItem = ({
  title, value, viewStyle, titleStyle, valueStyle, style,titleMedium=false,valueMedium=true
}) => {
  const {colors} = useTheme();

  const titleStyles = {
    ...styles.margin,
    ...titleStyle,
  };
  const valueStyles = {
    ...styles.value,
    ...valueStyle,
  };

  return (
    <View style={[styles.container, viewStyle, style]}>
      <NPAText
        text={title + ' :'}
        textColor={colors.lightText}
        style={titleStyles}
        medium={titleMedium}
      />
      <NPAText
        text={value}
        textColor={colors.teleHeader}
        medium={valueMedium}
        style={valueStyles}
      />
    </View>
  );
};

export default TextItem;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 3,
  },
  margin: {
    marginEnd: 5,
    fontSize: 12,
    alignSelf: 'center',
  },
  value: {
    fontSize: 14,
    alignSelf: 'center',
    flex: 1,
  },
});
