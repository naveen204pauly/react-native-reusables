import { useTheme } from '@react-navigation/native'
import React from 'react'
import { View, StyleSheet, Image, TouchableOpacity } from 'react-native'
import { Icon } from '../../../assets/icon';
import NPAText from '../npa-text';
import Dash from 'react-native-dash';
import { strings } from '../../localization';

const ThreeButtons = ({
    onPressCall,
    onPressLocation,
    onPressAssign,
    containerStyle,
    isAssigned = false
}) => {
    const { colors } = useTheme();
    const styles = style(colors)
    return (
        <View style={[styles.container, containerStyle]}>
            <TouchableOpacity style={[styles.item, { justifyContent: 'flex-start', marginLeft: 20, marginRight: 30, flex: 0.5 }]} onPress={onPressCall}>
                <Image source={Icon.bmCall}
                    style={styles.icon}
                    resizeMode={'contain'}
                />
                <NPAText
                    text={strings.call}
                    textColor={colors.white}
                />
            </TouchableOpacity>
            <Dash dashColor={'#D0D7E5'} style={styles.verticalDashedLine} />
            <TouchableOpacity style={[styles.item, { marginHorizontal: 20 }]} onPress={onPressLocation}>
                <Image source={Icon.bmLocation}
                    style={styles.icon}
                    resizeMode={'contain'}
                />
                <NPAText
                    text={strings.viewLocation}
                    textColor={colors.white}
                />
            </TouchableOpacity>
            <Dash dashColor={'#D0D7E5'} style={styles.verticalDashedLine} />
            <TouchableOpacity style={[styles.item, { marginHorizontal: 20 }]} onPress={onPressAssign} disabled={isAssigned}>
                {isAssigned ?
                    <>
                        <Image source={Icon.caseAssigned}
                            style={[styles.icon, {tintColor: '#FFB8B2'}]}
                            resizeMode={'contain'}
                        />
                        <NPAText
                            text={strings.caseAssigned}
                            textColor={colors.white}
                        />
                    </> :
                    <>
                        <Image source={Icon.bmAssignCase}
                            style={styles.icon}
                            resizeMode={'contain'}
                        />
                        <NPAText
                            text={strings.assignCase}
                            textColor={colors.white}
                        />
                    </>
                }
            </TouchableOpacity>
        </View>
    )
}

export default ThreeButtons;

const style = (props) => StyleSheet.create({
    container: {
        height: 40,
        borderRadius: 25,
        backgroundColor: props.teleHeader,
        marginVertical: 10,
        flexDirection: 'row'
    },
    item: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'row',
        alignSelf: 'center',
        justifyContent: 'center'
    },
    icon: {
        width: 15,
        height: 15,
        marginEnd: 5,
        tintColor: props.white
    },
    verticalDashedLine: {
        width: StyleSheet.hairlineWidth,
        height: '65%',
        flexDirection: 'column',
        alignSelf: 'center'
    },
})
