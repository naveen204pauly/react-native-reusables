import React from 'react';
import { View, StyleSheet, TouchableOpacity, Image } from 'react-native';
import NPAText from '../npa-text';
import { Icon } from '../../../assets/icon';
import { TextInput } from 'react-native-gesture-handler';
import { strings } from '../../localization';

const BMUpdateNumber = ({ style, onChangeText, value }) => {
    return (
        <View style={[styles.container, style]}>
            <NPAText text={strings.updateMobileNumber} textColor={'#162D4E'} />
            <View style={styles.underLineStyle}>
                <TextInput
                    placeholder={''}
                    style={styles.textField}
                    editable={true}
                    value = {value}
                    onChangeText={onChangeText}
                />
            </View>
        </View>
    );
};

export default BMUpdateNumber;

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 5
    },
    underLineStyle: {
        borderBottomWidth: 1,
        borderColor: '#162D4E',
        flexDirection: 'row',
    },
    textField: {
        flex: 1,
        height: 30,
        padding: 0
    }
});
