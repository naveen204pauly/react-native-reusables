import React, { useState } from 'react';
import { View, TouchableOpacity, Image, StyleSheet } from 'react-native';
import { Icon } from '../../../assets/icon';
import NPAText from '../npa-text';
import BMAudioView from '../audio-view/audio-view';
import { strings } from '../../localization';

const BMCaptureImage = ({ navigation,openCamera  }) => {
    return (
        <View style={styles.container}>
            <View style={{ flexDirection: 'row' }}>
                <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }} 
                onPress={openCamera}>
                    <View style={styles.buttonView}>
                        <Image source={Icon.camera} style={styles.buttonIcon} />
                    </View>
                    <NPAText text={'Capture'} style={styles.buttonText} textColor={'#162D4E'} />
                </TouchableOpacity>
                <NPAText text={'(or)'}  style={styles.orText} textColor={'#162D4E'}/>
                <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <View style={styles.buttonView}>
                        <Image source={Icon.upload} style={styles.buttonIcon} />
                    </View>
                    <NPAText text={'Upload'} style={styles.buttonText} textColor={'#162D4E'}/>
                </TouchableOpacity>
            </View>
        </View>
    );
};

export default BMCaptureImage;

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        borderRadius: 3,
        borderWidth: 1,
        borderStyle: 'dashed',
        borderColor: '#16285264',
        marginHorizontal: 10,
        backgroundColor: '#FFFFFFA2',
        padding: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonView: {
        height: 25,
        width: 25,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 50,
        backgroundColor: '#162852'
    }, 
    buttonIcon:{
        width: 10, 
        height: 10
    },
    buttonText:{
        marginLeft: 5,
        // fontSize: 12
    },
    orText:{
        marginHorizontal: 20, 
        // fontSize: 12, 
        alignSelf: 'center'}
});
