import React from 'react';
import {View, StyleSheet, Image, TouchableOpacity} from 'react-native';
import NPAText from 'src/component/npa-text';
import {useTheme} from '@react-navigation/native';
import {Icon} from 'assets/icon';
const CheckBox = ({
  containerStyle,
  isSelected,
  selectedImage = Icon.checkMarkSelected,
  unSelectedImage = Icon.checkMarkUnSelected,
  title,
  didPressed,
}) => {
  const {font, colors} = useTheme();
  return (
    <TouchableOpacity
      style={[styles.container, containerStyle]}
      onPress={didPressed}>
      <Image
        resizeMode={'contain'}
        source={isSelected ? selectedImage : unSelectedImage}
        style={{width: 15, height: 15, marginRight: 10}}
      />
      <NPAText text={title} textColor={colors.lightText} />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  titleStyle: {
    position: 'absolute',
    top: -10,
    left: 10,
    paddingHorizontal: 5,
  },
});

export default CheckBox;
