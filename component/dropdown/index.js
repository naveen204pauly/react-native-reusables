import React, {useEffect, useState, useRef} from 'react';
import {
  View,
  Text,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  Image,
  Modal,
  FlatList,
  TouchableWithoutFeedback,
  Dimensions,
} from 'react-native';
import {Icon} from '../../../assets/icon';
import {strings} from '../../localization';
import NPAText from '../npa-text';
import Orientation from 'react-native-orientation-locker';
import {useTheme} from '@react-navigation/native';

const Dropdown = ({
  title,
  style,
  isBlur,
  full = false,
  data,
  onSelectedValue,
  value,
  fieldKey,
  icon=Icon.dropdown,
  placeHolder
}) => {
  const {font} = useTheme();
  const [selected, setSelected] = useState(false);
  const [isLandscape, setIsLandscape] = useState(false);
  const ref = useRef();

  const [size, setSize] = useState({});
  /* find the window layout */
  useEffect(() => {
    ref.current.measureInWindow((x, y, width, height) => {
      setSize({width, height, x, y});
      // console.log(size);
    });
    Orientation.getOrientation((res) => {
      // console.log('Orentation', res);
      if(res.includes('LANDSCAPE')){
        if(!isLandscape){
          setIsLandscape(true)
        }
      }else{
        if(isLandscape){
          setIsLandscape(false)
        }
      }
    });
  }, [size]);


  const titleColor = {
    color: '#4a4a4a',
  };


  const capitalize = (s) => {
    if (typeof s !== 'string') return ''
      let str = s.toLowerCase().replace('_',' ')
    return str.charAt(0).toUpperCase() + str.slice(1)
  }


  const renderDropdown = () => {
    if (selected) {
      // console.log(size);

      return (
        <Modal
          visible={selected}
          transparent={true}
          onRequestClose={() => {
            setSelected(false);
          }}>
          <TouchableWithoutFeedback onPress={() => setSelected(false)}>
            <View style={{flex: 1}}>
              <View
                style={[
                  styles.dropdown,
                  {
                    top: size.y + size.height + 3,
                    width: size.width + 2,
                  },
                  {start:  isLandscape?size.x -28:size.x-1},
                ]}>
                <FlatList
                  data={data}
                  style={[
                    {
                      flex: 1,
                      maxHeight: full ? data.length * 40 : 200,
                    },
                  ]}
                  persistentScrollbar={true}
                  keyExtractor={(item, index) => index.toString()}
                  renderItem={(datas) => {
                    // console.log("Item",key);
                    return (
                      <TouchableOpacity
                        style={[
                          {
                            // width: '100%',
                            height: 40,
                            justifyContent: 'center',
                            paddingHorizontal: 5,
                            marginHorizontal: 10,
                            borderBottomWidth: 1,
                            borderColor: '#E4E9F4'
                          },
                          value === datas.item && {
                            backgroundColor: 'rgba(170, 211, 255, 0.3)',
                          },
                        ]}
                        onPress={() => {
                          onSelectedValue(datas.item);
                          console.log(datas.item);
                          setSelected(false);
                        }}>
                        <Text
                          style={[
                            {color: '#162852', fontWeight: font.medium,},
                            value!==null && value.hasOwnProperty(fieldKey) &&
                              value[fieldKey] === datas.item[fieldKey] && {
                                fontWeight: 'bold',
                              },
                          ]}>
                          {capitalize(datas.item[fieldKey]).replace('_',' ')}
                        </Text>
                      </TouchableOpacity>
                    );
                  }}
                />
              </View>
            </View>
          </TouchableWithoutFeedback>
        </Modal>
      );
    }
  };

  return (
    <View style={{paddingTop: 10}}>
      <View style={[styles.container, style, isBlur && {opacity: 0.3}]}>
        <TouchableOpacity
          ref={ref}
          style={styles.rowContainer}
          onPress={() => setSelected(!selected)}>
          <View style={{flex: 1}}>
            <NPAText
              text={
                value === null
                  ? placeHolder
                  : value.hasOwnProperty(fieldKey)
                  ? capitalize(value[fieldKey].replace('_',' '))
                  : value
              }
              textColor={value === '' || value===null ? '#939AAC' : '#162D4E'}
            />
          </View>
          <Image
            source={icon}
            style={[
              styles.downArrowStyle,
              {
                marginStart: 8,
              },
            ]}
            tintColor={'#8B9BBC'}
            resizeMode={'contain'}
          />
        </TouchableOpacity>
        {renderDropdown()}
      </View>
      {title && <Text style={[styles.titleStyle, titleColor]}>{title}</Text>}
    </View>
  );
};

export default Dropdown;

const styles = StyleSheet.create({
  container: {
    width: 150,
    borderWidth: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    borderColor: '#D0D7E5',
    borderRadius:3,
    minHeight: 40,
    backgroundColor: '#FFFFFF',
  },
  titleStyle: {
    // position: 'absolute',
    start: 10,
    position: 'absolute',
    fontWeight: 'bold',
    color: '#5E626A',
  },
  rowContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
  },
  downArrowStyle: {
    width: 15,
    height: 15,
    tintColor: '#7E7E7E',
  },
  dropdown: {
    width: '100%',
    elevation: 5,
    position: 'absolute',
    backgroundColor: '#ffffff',
    shadowColor: 'rgba(0, 0, 0, 0.16)',
    shadowOffset: {width: 0, height: 0},
    shadowOpacity: 0.4,
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: 'rgba(0, 0, 0, 0.16)',
    borderRadius: 3,
  },
});
