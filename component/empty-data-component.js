import React from 'react';
import {View, StyleSheet, Image, TouchableOpacity} from 'react-native';
import NPAText from 'src/component/npa-text';
import {useTheme} from '@react-navigation/native';
import {Icon} from 'assets/icon';
const EmptyData = ({containerStyle, title, description}) => {
  const {font, colors} = useTheme();
  return (
    <View style={[styles.container, containerStyle]}>
      <Image
        resizeMode={'contain'}
        style={styles.imageStyle}
        source={Icon.noRecordFound}
      />
      <NPAText
        medium
        text={title}
        textColor={'#4A4F5C'}
        style={{marginBottom: 15}}
      />
      <NPAText text={description} textColor={'#6A6A6A'} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageStyle: {
    height: 150,
    aspectRatio: 1729 / 700,
  },
});

export default EmptyData;
