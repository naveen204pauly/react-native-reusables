// import React from 'react';
// import {Text, View, StyleSheet, TextInput, Image} from 'react-native';
// import NPAText from 'src/component/npa-text';
// import {useTheme} from '@react-navigation/native';
// import {Icon} from 'assets/icon';
// const FilterView = ({containerStyle, title, value}) => {
//   const {font, colors} = useTheme();
//   return (
//     <View style={[styles.container, containerStyle]}>
//       <View style={{flexDirection: 'row', alignItems: 'center'}}>
//         <NPAText text={value} textColor={'#9CA3B1'} style={{flex: 1}} />
//         <Image
//           resizeMode={'contain'}
//           source={Icon.dropDown}
//           style={{width: 12, height: 12}}
//         />
//       </View>
//       <View style={styles.titleStyle}>
//         <NPAText
//           medium
//           text={title}
//           textColor={'#5E626A'}
//           style={{fontSize: 13}}
//         />
//       </View>
//     </View>
//   );
// };

// const styles = StyleSheet.create({
//   container: {
//     height: 45,
//     borderColor: '#E4E5EA',
//     borderWidth: 1,
//     borderRadius: 4,
//     justifyContent: 'center',
//     backgroundColor: 'white',
//     marginVertical: 15,
//     padding: 10,
//   },
//   titleStyle: {
//     position: 'absolute',
//     top: -10,
//     left: 10,
//     paddingHorizontal: 5,
//   },
// });

// export default FilterView;


import React from 'react';
import {Text, View, StyleSheet, Platform, Image} from 'react-native';
import {useTheme} from '@react-navigation/native';
import {Icon} from 'assets/icon';
import DropDownPicker from 'react-native-dropdown-picker';

const FilterView = ({
  containerStyle,
  title,
  value,
  onChangeItem,
  placeHolder,
  zIndex,
  data,
}) => {
  const {font, colors} = useTheme();
  const dropDownImage = () => {
    return (
      <Image
        source={Icon.dropDown}
        style={{width: 12, height: 12}}
        resizeMode={'contain'}
      />
    );
  };
  return (
    <View style={[styles.container, containerStyle]}>
      <View style={styles.titleStyle}>
        <Text
          style={{fontWeight: '700', fontSize: 13, color: 'gray'}}>
          {title}
        </Text>
      </View>
      <View
        style={{
          // The solution: Apply zIndex to any device except Android
          ...(Platform.OS !== 'android' && {
            zIndex: zIndex,
          }),
        }}>
        <DropDownPicker
          items={data}
          arrowStyle={{marginRight: 10}}
          labelStyle={{color: '#9CA3B1'}}
          // activeItemStyle={{backgroundColor: 'rgba(170, 211, 255, 0.3)'}}
          // activeLabelStyle={{fontWeight: '700'}}
          customArrowUp={dropDownImage}
          customArrowDown={dropDownImage}
          placeholder={placeHolder}
          placeholderStyle={{color: '#9b9a9b'}}
          containerStyle={{height: 40}}
          style={styles.dropDownContainerStyle}
          itemStyle={styles.itemStyle}
          // selectedLabelStyle={{fontWeight: 'bold'}}
          dropDownStyle={styles.dropDownStyle}
          onChangeItem={onChangeItem}
          zIndex={zIndex}
          dropDownMaxHeight={250}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 45,
    marginVertical: 15,
  },
  titleStyle: {
    position: 'absolute',
    top: -10,
    left: 10,
    paddingHorizontal: 5,
    // backgroundColor: 'white',
    zIndex: 2001,
  },
  dropDownStyle: {
    backgroundColor: 'white',
    elevation: 5,
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3,
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3,
    marginTop: 0,
    paddingHorizontal: 0,
  },
  itemStyle: {
    justifyContent: 'flex-start',
    width: '120%',
    paddingHorizontal: 10,
    height: 45,
  },
  dropDownContainerStyle: {
    borderColor: '#E4E5EA',
    borderWidth: 1,
    borderRadius: 5,
    paddingLeft: 10,
  },
});

export default FilterView;
