import React from 'react';
import {View, StyleSheet, Image, TouchableOpacity} from 'react-native';
import NPAText from 'src/component/npa-text';
import {useTheme} from '@react-navigation/native';
import {Icon} from 'assets/icon';
import DateInput from 'src/component/form-component/date-input';
import TimeInput from 'src/component/form-component/time-input';
import FreeTextInput from 'src/component/form-component/free-text-input';
const AddTask = ({
  containerStyle = {},
  title,
  enableSaveTask = true,
  enableViewDocument = false,
  saveTaskPressed,
  viewDocumentPressed,
  listenAudioPressed,
}) => {
  const {colors} = useTheme();
  const underlineButton = (buttonTitle, action) => {
    return (
      <TouchableOpacity
        style={{marginVertical: 10, marginRight: 15}}
        onPress={action}>
        <NPAText
          textColor={colors.darkText}
          style={{textDecorationLine: 'underline'}}
          text={buttonTitle}
        />
      </TouchableOpacity>
    );
  };

  const saveTaskButton = () => {
    if (enableSaveTask) {
      return underlineButton('Save task', saveTaskPressed);
    }
  };
  const renderViewDocument = () => {
    if (enableViewDocument) {
      return (
        <View style={{flexDirection: 'row', marginLeft: 100}}>
          {underlineButton('View Documents', viewDocumentPressed)}
          {underlineButton('Listen Audio', listenAudioPressed)}
        </View>
      );
    }
  };

  return (
    <View style={[styles.container, containerStyle]}>
      <FreeTextInput title={'Task Name'} horizontal />
      <FreeTextInput title={'Amount to be paid'} horizontal />
      <DateInput title={'Date'} horizontal />
      <TimeInput title={'Time'} horizontal />
      <FreeTextInput title={'Remarks'} horizontal />
      {saveTaskButton()}
      {renderViewDocument()}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    borderRadius: 8,
    backgroundColor: 'white',
    marginVertical: 5,
    padding: 10,
    borderWidth: 1,
    borderColor: '#D2D4E0',
  },
  titleContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});

export default AddTask;
