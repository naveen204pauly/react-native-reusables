import React, {useEffect} from 'react';
import {View, StyleSheet, Image, TextInput} from 'react-native';
import {NPAText} from 'src/component';
import {useTheme} from '@react-navigation/native';
import {Icon} from 'assets/icon';
import DropDownPicker from 'react-native-dropdown-picker';
import {CheckBox} from 'src/component';

const CheckBoxInput = ({
  containerStyle,
  choices = [],
  didSelected,
  chosenAnswer,
}) => {
  const {font, colors} = useTheme();
  console.log("CheckBoxInput",choices,didSelected,chosenAnswer);
  return (
    <View style={[styles.container, containerStyle]}>
      <View style={styles.checkBoxContainer}>
        {choices.map((value, index) => (
          <CheckBox
            containerStyle={{marginRight: 10, marginTop: 10}}
            title={value.choice}
            isSelected={chosenAnswer?.choice === value.choice}
            didPressed={() => didSelected(value)}
          />
        ))}
        {/*<CheckBox containerStyle={{marginRight: 10}} title={'Yes'} />*/}
        {/*<CheckBox title={'No'} isSelected={true} />*/}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {},
  calendarImageStyle: {
    width: 18,
    height: 18,
    tintColor: '#D2D4E0',
  },
  timeInputStyle: {
    padding: 0,
    width: 50,
    borderBottomWidth: 1,
    borderColor: '#D2D4E0',
    marginRight: 10,
    textAlign: 'center',
  },
  checkBoxContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10,
    flexWrap: 'wrap',
  },
});

export default CheckBoxInput;
