import React, {useState} from 'react';
import {
  View,
  StyleSheet,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import {NPAText} from 'src/component';
import {useTheme} from '@react-navigation/native';
import DateTimePicker from '@react-native-community/datetimepicker';
import {Icon} from 'assets/icon';
const DateInput = ({containerStyle, title, value, horizontal}) => {
  const {font, colors} = useTheme();
  const [showDatePicker, setShowDatePicker] = useState(false);
  const [selectedDate, setSelectedDate] = useState(null);

  const style = horizontal ? styles.horizontalContainer : styles.container;
  const renderHorizontally = () => {
    return (
      <>
        <NPAText
          text={title}
          medium
          style={{width: 90, marginRight: 10}}
          textColor={colors.darkText}
        />
        <View style={styles.underLineStyle}>{renderDateInput()}</View>
      </>
    );
  };

  const renderDateInput = () => {
    return (
      <>
        <TextInput
          placeholder={'DD/MM/YYYY'}
          style={{flex: 1, height: 30, padding: 0}}
          editable={false}
        />
        <Image
          resizeMode={'contain'}
          source={Icon.calendar}
          style={styles.calendarImageStyle}
        />
      </>
    );
  };

  const renderContent = () => {
    if (horizontal) {
      return renderHorizontally();
    }
    return renderDateInput();
  };
  return (
    <TouchableOpacity
      style={[style, containerStyle]}
      onPress={() => {
        setShowDatePicker(true);
      }}>
      {renderContent()}
      {showDatePicker && (
        <DateTimePicker
          testID="dateTimePicker"
          value={selectedDate ?? new Date()}
          mode={'date'}
          display="default"
          onChange={(event, date) => {
            setShowDatePicker(false);
            setSelectedDate(date);
          }}
        />
      )}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: '#D2D4E0',
    width: '80%',
    marginVertical: 10,
  },
  calendarImageStyle: {
    width: 18,
    height: 18,
    tintColor: '#D2D4E0',
  },
  horizontalContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 10,
  },
  underLineStyle: {
    borderBottomWidth: 1,
    borderColor: '#D2D4E0',
    flex: 1,
    flexDirection: 'row',
  },
});

export default DateInput;
