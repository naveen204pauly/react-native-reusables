import React, {useEffect, useState} from 'react';
import {
  View,
  StyleSheet,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import {NPAText} from 'src/component';
import {useTheme} from '@react-navigation/native';
import {Icon} from 'assets/icon';
import DateTime from '@react-native-community/datetimepicker';
import moment from 'moment';
import {times} from 'lodash';

const DateTimePicker = ({
  dateTitle,
  timeTitle,
  dateValue,
  timeValue,
  didSelected,
}) => {
  const [mode, setMode] = useState('Date');
  const [show, setShow] = useState(false);

  const [date, setDate] = useState(false);
  const [value, setValue] = useState(null);
  const [time, setTIme] = useState(false);

  useEffect(() => {
    console.log('Date', date, time);
    if (date && time) {
      didSelected(value);
    }
  }, [date, time, value]);

  const {font, colors} = useTheme();
  return (
    <View style={styles.container}>
      <View style={styles.alignCenter}>
        {/* date picker */}
        <NPAText
          text={dateTitle}
          style={{width: 120}}
          textColor={colors.lightText}
          numberOfLines={2}
        />
        <TouchableOpacity
          onPress={() => {
            setMode('date');
            setShow(true);
          }}
          style={styles.inputStyle}>
          <View style={styles.inputContainer}>
            <NPAText
              text={
                value === null || !date
                  ? 'DD-MMM-YYYY'
                  : moment(value).format('DD-MMM-YYYY')
              }
              placeholder={'dd-mmm-yyyy'}
              style={{
                flex: 1,
                color:
                  value === null && !date ? colors.lightText : colors.darkText,

                alignItems: 'center',
                justifyContent: 'center',
              }}
              textColor={
                value === null || !date ? colors.lightText : colors.darkText
              }
              editable={false}
            />
            <Image
              source={Icon.teleCalendar}
              style={styles.calendarImageStyle}
              resizeMode={'contain'}
            />
          </View>
        </TouchableOpacity>
      </View>
      <View style={styles.alignCenter}>
        {/* time picker */}
        <NPAText
          text={timeTitle}
          style={{width: 120}}
          textColor={colors.lightText}
        />
        <TouchableOpacity
          onPress={() => {
            console.log('time picker action');
            setMode('time');
            setShow(true);
          }}
          style={[styles.inputStyle, {width: 120}]}>
          <View style={styles.inputContainer}>
            <NPAText
              text={
                value === null || !time
                  ? 'HH:MM'
                  : moment(value).format('hh:mm A')
              }
              placeholder={'00:00 AM'}
              style={{
                flex: 1,
                color: value === null && !time ? 'grey' : colors.darkText,
                alignItems: 'center',
                justifyContent: 'center',
              }}
              textColor={
                value === null || !time ? colors.lightText : colors.darkText
              }
              editable={false}
            />
            <Image
              source={Icon.teleClock}
              style={styles.calendarImageStyle}
              resizeMode={'contain'}
            />
          </View>
        </TouchableOpacity>
      </View>
      {show && (
        <DateTime
          testID="dateTimePicker"
          value={value ? value : new Date()}
          mode={mode}
          minimumDate={new Date()}
          display="default"
          onChange={(event, date) => {
            if (date === undefined || date === null) {
              setShow(false);
              return;
            }
            
            setShow(false);
            console.log('Date', event, date);
            setValue(date);
            if (mode === 'date') {
              setDate(true);
            } else {
              setTIme(true);
            }
            console.log('Value', moment(date).day());
            // setShow(date);
          }}
        />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginTop: 10,
  },
  calendarImageStyle: {
    width: 18,
    height: 18,
    tintColor: '#8196B4',
  },
  inputStyle: {
    width: 180,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: '#D2D4E0',
    textAlign: 'center',
    marginHorizontal: 15,
    borderRadius: 5,
    height: 40,

    justifyContent: 'center',
    alignItems: 'center',
  },
  alignCenter: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
});

export default DateTimePicker;
