import React from 'react';
import {View, StyleSheet, Image, TextInput} from 'react-native';
import {NPAText} from 'src/component';
import {useTheme} from '@react-navigation/native';
import {Icon} from 'assets/icon';
import DropDownPicker from 'react-native-dropdown-picker';

const DropDownInput = ({
  containerStyle,
  title,
  value,
  placeHolder,
  titleStyle,
  dropDownContainerStyle,
  dropDownData,
  choices,
  didSelected,
  findKey='choice'
}) => {
  const {font, colors} = useTheme();

  const didSelectValue = (item) => {
    let chosenAnswer = item.value;
    let answer = choices.find((e) => e[findKey] == chosenAnswer);
    if (answer) {
      didSelected(answer);
    }
  };
  return (
    <View style={[styles.container, containerStyle]}>
      <NPAText text={title} style={titleStyle} textColor={colors.lightText} />
      <DropDownPicker
        items={dropDownData}
        customArrowUp={() => (
          <Image
            source={Icon.downArrow}
            style={{width: 10, height: 10}}
            resizeMode={'contain'}
          />
        )}
        customArrowDown={() => (
          <Image
            source={Icon.downArrow}
            style={{width: 10, height: 10}}
            resizeMode={'contain'}
          />
        )}
        placeholder={placeHolder}
        containerStyle={[{height: 35, flex: 1}, dropDownContainerStyle]}
        style={{
          backgroundColor: '#FFFFFF',
          borderWidth: 1,
          borderColor: '#D2D4E0',
          // marginTop: 10,
        }}
        itemStyle={{
          justifyContent: 'flex-start',
        }}
        dropDownStyle={{backgroundColor: '#FFFFFF'}}
        onChangeItem={didSelectValue}
        zIndex={2000}
        defaultValue={value}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {},
  calendarImageStyle: {
    width: 18,
    height: 18,
    tintColor: '#D2D4E0',
  },
  timeInputStyle: {
    padding: 0,
    width: 50,
    borderBottomWidth: 1,
    borderColor: '#D2D4E0',
    marginRight: 10,
    textAlign: 'center',
  },
});

export default DropDownInput;
