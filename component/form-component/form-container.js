import React from 'react';
import {View, StyleSheet, Image, TouchableOpacity} from 'react-native';
import {NPAText} from 'src/component';
import {useTheme} from '@react-navigation/native';
import {Icon} from 'assets/icon';
const FormContainer = ({
  containerStyle = {},
  title,
  buttonRequired,
  savePressed,
  nextPressed,
  children,
}) => {
  const {colors} = useTheme();

  const renderButton = () => {
    if (buttonRequired) {
      return (
        <View style={styles.actionButtonContainer}>
          <TouchableOpacity onPress={savePressed} style={styles.buttonStyle}>
            <NPAText medium text={'Save'} textColor={colors.darkText} />
          </TouchableOpacity>

          <TouchableOpacity
            style={[
              styles.buttonStyle,
              {borderColor: 0, backgroundColor: '#FFB8B2'},
            ]}
            onPress={nextPressed}>
            <NPAText medium text={'Next'} textColor={colors.darkText} />
          </TouchableOpacity>
        </View>
      );
    }
  };

  return (
    <View style={[styles.container, containerStyle]}>
      {/*<View style={styles.titleContainer}>*/}
      {/*  <NPAText*/}
      {/*    text={title}*/}
      {/*    medium*/}
      {/*    textColor={colors.title}*/}
      {/*    style={{flex: 1, fontSize: 15}}*/}
      {/*  />*/}
      {/*  <Image*/}
      {/*    source={Icon.downArrow}*/}
      {/*    style={{width: 12, height: 12}}*/}
      {/*    resizeMode={'contain'}*/}
      {/*  />*/}
      {/*</View>*/}
      {children}
      {renderButton()}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    borderRadius: 8,
    elevation: 10,
    backgroundColor: 'white',
    margin: 20,
    flex: 1,
    padding: 10,
  },
  titleContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 10,
  },
  actionButtonContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  buttonStyle: {
    borderColor: '#162D4E80',
    borderWidth: 1,
    width: 90,
    height: 30,
    backgroundColor: '#162D4E1A',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 3,
    marginRight: 10,
    marginTop: 10,
  },
});

export default FormContainer;
