import React from 'react';
import {View, StyleSheet, Image, TouchableOpacity} from 'react-native';
import NPAText from 'src/component/npa-text';
//Importing directly from the index file will cause a require cycle warning
import DateInput from 'src/component/form-component/date-input';
import TimeInput from 'src/component/form-component/time-input';
import FreeTextInput from 'src/component/form-component/free-text-input';
import DropDownInput from 'src/component/form-component/drop-down-input';
import CheckBoxInput from 'src/component/form-component/check-box-input';
import {useTheme} from '@react-navigation/native';
import {Icon} from 'assets/icon';
const FormInput = ({containerStyle = {}, title, form}) => {
  const {colors} = useTheme();

  const renderInput = (formType) => {
    if (formType == 'Date') {
      return <DateInput />;
    } else if (formType == 'Time') {
      return <TimeInput />;
    } else if (formType === 'TextBox') {
      return <FreeTextInput />;
    } else if (formType === 'DropDown') {
      return <DropDownInput />;
    } else if (formType === 'CheckBox') {
      return <CheckBoxInput />;
    }
  };
  const zIndexStyle =
    form.InputType === 'DropDown' || form.InputType === 'Time'
      ? {zIndex: 1000}
      : {};

  return (
    <View style={[styles.container, containerStyle, zIndexStyle]}>
      <NPAText
        text={title}
        textColor={colors.darkText}
        style={{flex: 1, fontSize: 13}}
      />
      {renderInput(form.InputType)}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    borderRadius: 8,
    backgroundColor: 'white',
    marginVertical: 10,
    padding: 10,
    borderWidth: 1,
    borderColor: '#D2D4E0',
    flex: 1,
  },
  titleContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});

export default FormInput;
