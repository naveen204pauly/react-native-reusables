import React from 'react';
import {View, StyleSheet, Image, TouchableOpacity} from 'react-native';
import {NPAText} from 'src/component';
import {useTheme} from '@react-navigation/native';
import {Icon} from 'assets/icon';
import DropDownPicker from 'react-native-dropdown-picker';

const FormSelectionDropDown = ({
  containerStyle,
  title,
  value,
  onChangeItem,
  onPressStartRecording,
}) => {
  const {font, colors} = useTheme();

  const dropDownArrow = (expanded = false) => {
    return (
      <Image
        source={Icon.downArrow}
        style={{
          width: 10,
          height: 10,
          transform: [{rotate: expanded ? '-90deg' : '0deg'}],
        }}
        resizeMode={'contain'}
      />
    );
  };
  return (
    <View style={[styles.container, containerStyle]}>
      <TouchableOpacity
        style={styles.startRecordContainer}
        onPress={onPressStartRecording}>
        <Image
          resizeMode={'contain'}
          source={Icon.recordingMic}
          style={{width: 15, height: 15}}
        />
        <NPAText
          text={'Start recording'}
          style={{fontSize: 12, marginLeft: 5}}
        />
      </TouchableOpacity>
      <DropDownPicker
        items={[
          {
            label: 'Queries for loan Information',
            value: 'Queries for loan Information',
          },
          {
            label: 'NPA Queries to customers',
            value: 'NPA Queries to customers',
          },
          {
            label: 'Documents',
            value: 'Documents',
          },
          {
            label: 'Add a task',
            value: 'Add a task',
          },
        ]}
        customArrowUp={() => dropDownArrow(false)}
        customArrowDown={() => dropDownArrow(true)}
        containerStyle={{height: 45}}
        style={{borderWidth: 0, backgroundColor: '#ffffff', elevation: 11}}
        itemStyle={{justifyContent: 'flex-start'}}
        labelStyle={styles.dropdownLabelStyle}
        dropDownStyle={{backgroundColor: '#ffffff', elevation: 11}}
        onChangeItem={onChangeItem}
        zIndex={5000}
        defaultValue={'Queries for loan Information'}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {},
  calendarImageStyle: {
    width: 18,
    height: 18,
    tintColor: '#D2D4E0',
  },
  timeInputStyle: {
    padding: 0,
    width: 50,
    borderBottomWidth: 1,
    borderColor: '#D2D4E0',
    marginRight: 10,
    textAlign: 'center',
  },
  dropdownLabelStyle: {
    fontSize: 14,
    textAlign: 'left',
    color: '#4A4F5C',
    fontFamily: 'HelveticaNeueMedium',
  },
  startRecordContainer: {
    flexDirection: 'row',
    backgroundColor: '#D4D6E0',
    width: 130,
    height: 30,
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 1000,
    bottom: -8,
    marginLeft: 10,
    elevation: 12,
  },
});

export default FormSelectionDropDown;
