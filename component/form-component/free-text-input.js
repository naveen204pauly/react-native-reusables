import React from 'react';
import {View, StyleSheet, Image, TextInput} from 'react-native';
import {NPAText} from 'src/component';
import {useTheme} from '@react-navigation/native';
import {Icon} from 'assets/icon';
const FreeTextInput = ({
  containerStyle,
  title,
  value,
  horizontal,
  placeholder,
}) => {
  const {font, colors} = useTheme();

  const style = horizontal ? styles.horizontalContainer : styles.container;
  const horizontalTextInput = () => {
    return (
      <>
        <NPAText
          text={title}
          medium
          style={{width: 90, marginRight: 10}}
          textColor={colors.darkText}
        />
        <View style={styles.underLineStyle}>{textInput()}</View>
      </>
    );
  };
  const textInput = () => {
    return (
      <TextInput
        placeholder={placeholder}
        style={{flex: 1, height: 30, padding: 0}}
      />
    );
  };
  const renderContent = () => {
    if (horizontal) {
      return horizontalTextInput();
    }
    return textInput();
  };

  return <View style={[style, containerStyle]}>{renderContent()}</View>;
};

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    borderBottomWidth: 1,
    borderColor: '#D2D4E0',
    marginVertical: 10,
  },
  calendarImageStyle: {
    width: 18,
    height: 18,
    tintColor: '#D2D4E0',
  },
  horizontalContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 10,
  },

  underLineStyle: {
    borderBottomWidth: 1,
    borderColor: '#D2D4E0',
    flex: 1,
    flexDirection: 'row',
  },
});

export default FreeTextInput;
