import React from 'react';
import {View, StyleSheet, Image, TouchableOpacity} from 'react-native';
import {NPAText} from 'src/component';
import {useTheme} from '@react-navigation/native';
import {Icon} from 'assets/icon';
const KeyValueForm = ({containerStyle, title, value}) => {
  const {font, colors} = useTheme();
  return (
    <View style={[styles.container, containerStyle]}>
      <NPAText text={title} textColor={colors.title} style={{flex: 1}} medium />
      <NPAText
        text={':'}
        textColor={colors.title}
        style={{marginHorizontal: 10}}
      />
      <NPAText text={value} textColor={colors.darkText} style={{width: 130}} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 15,
    marginVertical: 15,
  },
});

export default KeyValueForm;
