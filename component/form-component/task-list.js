import React from 'react';
import {View, StyleSheet, Image, TouchableOpacity} from 'react-native';
import NPAText from 'src/component/npa-text';
import {useTheme} from '@react-navigation/native';
import {Icon} from 'assets/icon';
const TaskList = ({containerStyle = {}, title, form}) => {
  const {colors} = useTheme();

  return (
    <View style={[styles.container, containerStyle]}>
      <View
        style={{
          borderWidth: 1,
          borderColor: '#B9BDD5',
          paddingVertical: 18,
          paddingHorizontal: 10,
          flexDirection: 'row',
          borderStyle: 'dashed',
          borderRadius: 5,
        }}>
        <NPAText
          medium
          text={'Task : Installment 1 abccd house'}
          style={{flex: 1}}
          textColor={'#566E90'}
        />
        <Image
          resizeMode={'contain'}
          source={Icon.edit}
          style={{width: 15, height: 15}}
        />
      </View>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'flex-end',
          marginTop: 10,
        }}>
        <TouchableOpacity onPress={() => {}}>
          <NPAText
            textColor={colors.darkText}
            style={{textDecorationLine: 'underline', marginRight: 15}}
            text={'Add another task'}
          />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => {}}>
          <NPAText
            textColor={colors.darkText}
            style={{textDecorationLine: 'underline'}}
            text={'Delete task'}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    borderRadius: 8,
    backgroundColor: 'white',
    marginVertical: 5,
    padding: 10,
  },
  titleContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});

export default TaskList;
