import React from 'react';
import {View, StyleSheet, Image, TextInput} from 'react-native';
import {NPAText} from 'src/component';
import {useTheme} from '@react-navigation/native';
import {Icon} from 'assets/icon';
import DropDownPicker from 'react-native-dropdown-picker';

const TimeInput = ({containerStyle, title, value, horizontal}) => {
  const {font, colors} = useTheme();

  const renderHorizontally = () => {
    return (
      <View style={[styles.horizontalContainer, containerStyle]}>
        <NPAText
          text={title}
          medium
          style={{width: 90, marginRight: 10}}
          textColor={colors.darkText}
        />
        {timeInputComponent()}
      </View>
    );
  };

  const timeInputComponent = () => {
    return (
      <View style={{flexDirection: 'row', alignItems: 'center'}}>
        <TextInput
          keyboardType={'number-pad'}
          maxLength={2}
          style={styles.timeInputStyle}
        />
        <NPAText
          text={':'}
          style={{marginRight: 10, alignSelf: 'flex-end'}}
          textColor={'#9CA3B1'}
        />
        <TextInput
          keyboardType={'number-pad'}
          maxLength={2}
          style={styles.timeInputStyle}
        />
        <DropDownPicker
          items={[
            {
              label: 'AM',
              value: 'AM',
            },
            {
              label: 'PM',
              value: 'PM',
            },
          ]}
          customArrowUp={() => (
            <Image
              source={Icon.dropDown}
              style={{width: 10, height: 10}}
              resizeMode={'contain'}
            />
          )}
          customArrowDown={() => (
            <Image
              source={Icon.dropDown}
              style={{width: 10, height: 10}}
              resizeMode={'contain'}
            />
          )}
          defaultValue={'AM'}
          containerStyle={{height: 40, width: 65}}
          style={{backgroundColor: '#FFFFFF', borderWidth: 0}}
          itemStyle={{
            justifyContent: 'flex-start',
          }}
          dropDownStyle={{backgroundColor: '#FFFFFF'}}
          onChangeItem={(item) => {}}
          zIndex={1000}
        />
      </View>
    );
  };

  const renderContent = () => {
    if (horizontal) {
      return renderHorizontally();
    }
    return (
      <View style={[styles.container, containerStyle]}>
        <NPAText text={'Time'} textColor={'#9CA3B1'} style={{fontSize: 13}} />
        {timeInputComponent()}
      </View>
    );
  };

  return renderContent();
};

const styles = StyleSheet.create({
  container: {},
  calendarImageStyle: {
    width: 18,
    height: 18,
    tintColor: '#D2D4E0',
  },
  timeInputStyle: {
    padding: 0,
    width: 40,
    borderBottomWidth: 1,
    borderColor: '#D2D4E0',
    marginRight: 10,
    textAlign: 'center',
  },
  horizontalContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 10,
  },
});

export default TimeInput;
