import React from 'react';
import {View, StyleSheet, Image, TouchableOpacity} from 'react-native';
import {NPAText} from 'src/component';
import {useTheme} from '@react-navigation/native';
import {Icon} from 'assets/icon';
const CameraCapture = ({containerStyle, title, value, uploadPressed}) => {
  const {font, colors} = useTheme();

  return (
    <View style={styles.rowContainer}>
      <TouchableOpacity style={styles.buttonContainer}>
        <Image
          resizeMode={'contain'}
          source={Icon.camera}
          style={{width: 20, height: 20}}
        />
      </TouchableOpacity>
      <NPAText
        text={'Capture'}
        style={styles.captureStyle}
        textColor={colors.lightText}
      />
      <NPAText
        text={'(OR)'}
        style={{marginHorizontal: 20, fontSize: 12}}
        textColor={colors.lightText}
      />
      <TouchableOpacity style={styles.buttonContainer} onPress={uploadPressed}>
        <Image
          resizeMode={'contain'}
          source={Icon.upload}
          style={{width: 20, height: 20}}
        />
      </TouchableOpacity>
      <NPAText
        text={'Upload'}
        style={styles.captureStyle}
        textColor={colors.lightText}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
  },
  calendarImageStyle: {
    width: 18,
    height: 18,
    tintColor: '#D2D4E0',
  },
  rowContainer: {
    borderColor: '#D2D4E0',
    borderWidth: 1,
    borderRadius: 5,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 10,
    marginVertical: 10,
  },
  buttonContainer: {
    width: 40,
    height: 40,
    backgroundColor: '#566E90',
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  captureStyle: {
    marginLeft: 10,
  },
});

export default CameraCapture;
