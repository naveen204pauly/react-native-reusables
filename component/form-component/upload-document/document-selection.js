import React from 'react';
import {View, StyleSheet, Image, TouchableOpacity} from 'react-native';
import {NPAText} from 'src/component';
import {useTheme} from '@react-navigation/native';
import {Icon} from 'assets/icon';
import * as Progress from 'react-native-progress';
const DocumentSelection = ({containerStyle, title, value, uploadPressed}) => {
  const {font, colors} = useTheme();

  return (
    <View style={[styles.selectedImageContainer, containerStyle]}>
      <Image source={Icon.profileThumbnail} style={styles.documentImageStyle} />
      <View style={{flex: 1, marginLeft: 10}}>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <NPAText
            text={'124323image.jpg'}
            textColor={colors.lightText}
            style={{fontSize: 13}}
          />
          <NPAText text={'100%'} textColor={'#5AA86C'} style={{fontSize: 13}} />
        </View>

        <View style={{height: 8, marginVertical: 10}}>
          <Progress.Bar
            progress={0.7}
            width={200}
            borderColor={'#5AA86C'}
            color={'#5AA86C'}
          />
        </View>
        <View style={{flexDirection: 'row', alignSelf: 'flex-end'}}>
          <TouchableOpacity>
            <NPAText
              text={'Replace'}
              style={{fontSize: 13, textDecorationLine: 'underline'}}
              textColor={'#162D4E'}
            />
          </TouchableOpacity>
          <TouchableOpacity style={{marginHorizontal: 10}}>
            <NPAText
              text={'View'}
              style={{fontSize: 13, textDecorationLine: 'underline'}}
              textColor={'#162D4E'}
            />
          </TouchableOpacity>
          <TouchableOpacity>
            <NPAText
              text={'Delete'}
              style={{fontSize: 13, textDecorationLine: 'underline'}}
              textColor={'#162D4E'}
            />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  selectedImageContainer: {
    borderStyle: 'dashed',
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#B9BDD5',
    flexDirection: 'row',
    alignItems: 'center',
    padding: 15,
  },
  documentImageStyle: {
    width: 50,
    height: 50,
    borderRadius: 25,
    borderColor: '#B9BDD5',
    borderWidth: 1,
  },
});

export default DocumentSelection;
