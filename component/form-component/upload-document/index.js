import React from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import {NPAText} from 'src/component';
import {useTheme} from '@react-navigation/native';
import CameraCapture from './camera-capture';
import DocumentSelection from './document-selection';
const UploadDocument = ({containerStyle, title, value, uploadPressed}) => {
  const {font, colors} = useTheme();

  return (
    <View style={[styles.container, containerStyle]}>
      <NPAText medium text={'Upload Documents'} textColor={'#566E90'} />
      <CameraCapture uploadPressed={uploadPressed} />
      <DocumentSelection />
      <TouchableOpacity style={{marginTop: 10, alignSelf:'flex-end'}}>
            <NPAText
              text={'Add another document'}
              style={{fontSize: 13, textDecorationLine: 'underline'}}
              textColor={'#162D4E'}
            />
          </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
  },
});

export default UploadDocument;
