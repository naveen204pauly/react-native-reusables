import React from 'react';
import {View, StyleSheet, Image, TouchableOpacity} from 'react-native';
import {NPAText} from 'src/component';
import {useTheme} from '@react-navigation/native';
import {Icon} from 'assets/icon';
import * as Progress from 'react-native-progress';
const ViewDocument = ({
  containerStyle,
  documentPressed,
}) => {
  const {font, colors} = useTheme();

  const renderUploadDocument = () => {
    return (
      <View style={styles.borderContainer}>
        <TouchableOpacity style={{marginBottom: 10}} onPress={documentPressed}>
          <NPAText
            text={'Loan information'}
            style={{fontSize: 13, textDecorationLine: 'underline'}}
            textColor={'#162D4E'}
          />
        </TouchableOpacity>
        <TouchableOpacity onPress={documentPressed}>
          <NPAText
            text={'Aadhar card'}
            style={{fontSize: 13, textDecorationLine: 'underline'}}
            textColor={'#162D4E'}
          />
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <View style={[styles.container, containerStyle]}>
      <NPAText medium text={'View Documents'} textColor={'#566E90'} />
      {renderUploadDocument()}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
  },
  calendarImageStyle: {
    width: 18,
    height: 18,
    tintColor: '#D2D4E0',
  },
  borderContainer: {
    borderColor: '#D2D4E0',
    borderWidth: 1,
    borderRadius: 5,
    justifyContent: 'center',
    padding: 10,
    marginVertical: 10,
  },
  buttonContainer: {
    width: 40,
    height: 40,
    backgroundColor: '#566E90',
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  captureStyle: {
    marginLeft: 10,
  },
  selectedImageContainer: {
    borderStyle: 'dashed',
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#B9BDD5',
    flexDirection: 'row',
    alignItems: 'center',
    padding: 15,
  },
  documentImageStyle: {
    width: 50,
    height: 50,
    borderRadius: 25,
    borderColor: '#B9BDD5',
    borderWidth: 1,
  },
  fileNameStyle: {},
});

export default ViewDocument;
