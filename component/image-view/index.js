import React, { useState } from 'react';
import { View, TouchableOpacity, Image, StyleSheet } from 'react-native';
import { Icon } from '../../../assets/icon';
import NPAText from '../npa-text';
import BMAudioView from '../audio-view/audio-view';
import { strings } from '../../localization';
import * as Progress from 'react-native-progress';

const BMImageView = ({ navigation, image = Icon.camera, imageName, percentage, onPressAddMore }) => {
    return (
        <View style={styles.container}>
            <View style={styles.innerContainer}>
                <Image source={image} style={styles.imageView}resizeMode={'cover'} resizeMethod={'scale'} />
                <View style={{ marginLeft: 10, flex: 1 }}>
                    <View style={styles.pencentageView}>
                        <View style={{ flex: 1 }}>
                            <View style={styles.sliderPercentageView}>
                                <NPAText text={'imgfbeb.jpge'} textColor={'#6A6A6A'} style={styles.text} />
                                <NPAText text={'100%'} textColor={'#5AA86C'} style={styles.text} />
                            </View>
                            {/* <View style={styles.sliderView}></View> */}
                            <Progress.Bar progress={0.5}  style={styles.sliderView} color={'#5AA86C'}/>
                        </View>
                        <View style={styles.deleteIocnView}>
                            <Image source={Icon.mic} style={styles.deleteIocn} />
                        </View>
                    </View>
                    <TouchableOpacity style={styles.addMoreView} onPress={onPressAddMore}>
                        <NPAText text={'Add another document'} style={styles.addMore} textColor={'#162D4E'} />
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    );
};

export default BMImageView;

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        borderRadius: 3,
        borderWidth: 1,
        borderStyle: 'dashed',
        borderColor: '#16285264',
        marginHorizontal: 10,
        backgroundColor: '#FFFFFFA2',
        padding: 10,
    },
    innerContainer: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    buttonView: {
        height: 25,
        width: 25,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 50,
        backgroundColor: '#162852'
    },
    imageView: {
        width: 50,
        height: 50,
        borderRadius: 50,
        borderWidth: 1,
        borderColor: '#D2D4E0',
    },
    text: {
        fontSize: 12,
    },
    deleteIocnView: {
        alignSelf: 'flex-end', marginLeft: 10
    },
    deleteIocn: {
        height: 15,
        width: 12,
        tintColor: '#162852',
    },
    addMoreView: {
        alignSelf: 'flex-end',
        marginTop: 10
    },
    addMore: {
        textDecorationLine: 'underline',
        textDecorationColor: '#162D4E',
        fontSize: 12
    },
    sliderView: {
        height: 6,
        width: '100%',
    },
    sliderPercentageView: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    pencentageView: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    }
});
