import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {MaterialIndicator} from 'react-native-indicators';
import ProgressCircle from 'react-native-progress-circle'

import {View, StyleSheet, Modal, Text} from 'react-native';

const Indicator = ({
  show,
  onRequestClose,
  isProgress = false,
  uploadProgress,
}) => {
  // console.log("Styles",style);

  return (
    <Modal
      animationType={'none'}
      transparent={true}
      // hardwareAccelerated={true}
      visible={show}>
      <View style={styles.container}>
        <View style={styles.centerViewStyle}>
          {isProgress ? (
            <ProgressCircle
            percent={uploadProgress}
            radius={30}
            borderWidth={8}
            color="#3399FF"
            shadowColor="#999"
            bgColor="#fff"
        >
            <Text style={{ fontSize: 18 }}>{uploadProgress+'%'}</Text>
        </ProgressCircle>
          ) : (
            <MaterialIndicator color={'#000000'} />
          )}
        </View>
      </View>
    </Modal>
  );
};

export default Indicator;
const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    top: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  centerViewStyle: {
    backgroundColor: 'white',
    justifyContent: 'center',
    width: 80,
    height: 80,
    borderRadius: 10,
    shadowColor: 'white',
    shadowOffset: {
      width: 3,
      height: 3,
    },
    shadowOpacity: 0.15,
    shadowRadius: 8,
    elevation: 8,
    alignItems:'center'
  },
});
