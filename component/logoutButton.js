import React, { useState } from 'react';
import { Image, StyleSheet, TouchableOpacity, View } from 'react-native';
import {CommonActions, useTheme} from '@react-navigation/native';
import { Icon } from 'assets/icon';
import AllowDenyPopup from 'src/pages/popup/allow-deny-popup';
import {UserManager} from 'src/storage';
import {navigate} from 'src/router/root-navigation';
import {Screen} from 'src/router/screen';
import {strings} from '../localization';
import Orientation from 'react-native-orientation-locker';

const LogoutButton = ({
  style,
  textColor,
  navigation,
  srcImage = Icon.logout,
}) => {
  const {font} = useTheme();
  const [showLogoutPopup, setShowLogoutPopup] = useState(false);

    return (
        <TouchableOpacity
            style={[styles.container]}
            onPress={() => {
                setShowLogoutPopup(true);
            }}>
            <Image
                style={[styles.imageStyle, textColor]}
                source={srcImage} />
            <AllowDenyPopup title={strings.areYouSureDesc+'?'}
                show={showLogoutPopup}
                allowPressed={() => {
                    UserManager.logout();
                    // navigate(Screen.login);
                    strings.setLanguage('en')
                    navigation.dispatch(
                        CommonActions.reset({
                            index: 0,
                            routes: [{name: Screen.login}],
                        }),
                    );
                    
                    setShowLogoutPopup(false);
                }
                }
                denyPressed={() => setShowLogoutPopup(false)}
            />
        </TouchableOpacity >
    );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    // borderBottomWidth: 1,
    borderColor: '#D2D4E0',
    width: '80%',
    marginEnd: 10,
  },
  imageStyle: {
    width: 28,
    height: 28,
    tintColor: '#D2D4E0',
  },
});

export default LogoutButton;
