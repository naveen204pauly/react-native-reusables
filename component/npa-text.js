import React from 'react';
import {StyleSheet, Text} from 'react-native';
import {useTheme} from '@react-navigation/native';

const NPAText = ({
  style,
  textColor,
  text,
  numberOfLines,
  medium,
  light,
  bold,
  thick,
  regular,
}) => {
  const {font} = useTheme();
  const textStyle = {
    ...styles.defaultStyle,
    ...style,
    ...{color: textColor, fontFamily: font.regular},
  };
  return (
    <Text
      style={[
        textStyle,
        medium && {fontFamily: font.medium},
        light && {fontFamily: font.light},
        bold && {fontFamily: font.bold},
        thick && {fontFamily: font.thick},
        regular && {fontFamily: font.regular},
      ]}
      numberOfLines={numberOfLines}>
      {text}
    </Text>
  );
};
const styles = StyleSheet.create({
  defaultStyle: {
    fontSize: 14,
    color: 'black',
  },
});

export default NPAText;
