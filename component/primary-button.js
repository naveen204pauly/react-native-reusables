import NPAText from './npa-text';
import {TouchableOpacity, StyleSheet} from 'react-native';
import React from 'react';

const PrimaryButton = ({onPress, title, textColor, containerStyle}) => {
  return (
    <TouchableOpacity onPress={onPress} style={[styles.buttonStyle, containerStyle]}>
      <NPAText medium text={title} textColor={textColor} />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  buttonStyle: {
    borderColor: '#162D4E80',
    borderWidth: 1,
    width: 110,
    height: 30,
    backgroundColor: '#162D4E1A',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 3,
  },
});

export default PrimaryButton;