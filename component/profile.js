import {Image, View, StyleSheet, TouchableOpacity} from 'react-native';
import {Icon} from 'assets/icon';
import React from 'react';

const ProfileComponent = ({onEditPressed, size=56, icon=Icon.userImage}) => {
  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={onEditPressed} style={styles.circleContainer}>
        <Image
          resizeMode={'contain'}
          source={icon}
          style={[styles.profileImageStyle, {width: size, height: size}]}
        />
        <View style={styles.editImageContainer}>
          <Image
            resizeMode={'contain'}
            source={Icon.edit}
            style={{width: 12, height: 12}}
          />
        </View>
      </TouchableOpacity>
    </View>
  );
};
export default ProfileComponent;

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 15,
  },
  circleContainer: {
    width: 60,
    height: 60,
    borderRadius: 30,
    backgroundColor: '#FFB8B2',
    justifyContent: 'center',
    alignItems: 'center',
  },
  editImageContainer: {
    backgroundColor: 'white',
    width: 20,
    height: 20,
    borderRadius: 10,
    borderColor: '#162D4E',
    borderWidth: 1,
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    right: -10,
    bottom: 10,
  },
  profileImageStyle: {
    width: 56,
    height: 56,
    // tintColor: '#162D4E',
  },
});
