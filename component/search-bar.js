import React, {useRef, useState} from 'react';
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  Image,
  TouchableOpacity,
} from 'react-native';
import {Icon} from 'assets/icon';
import NPAText from './npa-text';
const SearchBar = ({
  containerStyle,
  tagEnabled,
  value,
  placeHolder,
  onChangeText,
  onClear,
}) => {
  const [isFocused, setIsFocused] = useState(false);
  const inputRef = useRef(null);

  const renderTag = () => {
    if (tagEnabled && !isFocused && value) {
      return (
        <View style={styles.tagContainer}>
          <NPAText
            text={value}
            numberOfLines={2}
            regular
            textColor={'#636976'}
            style={{marginLeft: 10, marginRight: 10}}
          />
          <TouchableOpacity onPress={onClearPressed}>
            <Image
              source={Icon.close}
              style={{width: 12, height: 12, margin: 10}}
            />
          </TouchableOpacity>
        </View>
      );
    }
  };

  const onClearPressed = () => {
    inputRef.current.focus();
    onClear();
  };

  const searchValue = () => {
    if (tagEnabled && value != '' && !isFocused) {
      return '';
    }
    return value;
  };
  const searchPlaceHolder = () => {
    if (tagEnabled && value != '' && !isFocused) {
      return '';
    }
    return placeHolder;
  };

  return (
    <View style={[styles.container, containerStyle]}>
      {renderTag()}
      <TextInput
        ref={inputRef}
        style={styles.searchTextInput}
        value={searchValue()}
        placeholder={searchPlaceHolder()}
        onBlur={() => setIsFocused(false)}
        onFocus={() => setIsFocused(true)}
        onChangeText={onChangeText}
      />
      <View style={styles.searchIconContainer}>
        <Image source={Icon.search} style={{width: 15, height: 15}} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 45,
    borderColor: 'rgb(226,227,232)',
    borderWidth: 1,
    borderRadius: 25,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
    marginVertical: 15,
  },
  searchTextInput: {
    paddingHorizontal: 20,
    paddingVertical: 0,
    flex: 1,
    fontSize: 16,
  },
  searchIconContainer: {
    width: 38,
    height: 38,
    borderRadius: 19,
    backgroundColor: 'rgb(226,227,232)',
    right: 3,
    justifyContent: 'center',
    alignItems: 'center',
  },
  tagContainer: {
    height: 40,
    backgroundColor: '#E4E5EA',
    borderRadius: 20,
    marginLeft: 2,
    marginRight: 5,
    flexDirection: 'row',
    alignItems: 'center',
    // maxWidth: 150,
  },
});

export default SearchBar;
