import React from 'react';
import {View, StyleSheet, Text, Image} from 'react-native';
import {useTheme} from '@react-navigation/native';
import {Icon} from 'assets/icon';
import Toast from 'react-native-toast-message';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
export const ToastConfig = {
  success: () => {},
  error: () => {},
  info: (internalState) => <ToastComponent state={internalState} />,
};

export const CTToast = (text, type = 'info') => {
  Toast.show({
    type: type,
    position: 'top',
    text1: text,
    text2: '',
    visibilityTime: 2000,
    autoHide: true,
    topOffset: 60,
    bottomOffset: 0,
    onShow: () => {},
    onHide: () => {},
  });
  ReactNativeHapticFeedback.trigger('impactLight', {
    enableVibrateFallback: true,
    ignoreAndroidSystemSettings: false,
  });
};

const ToastComponent = ({state}) => {
  const {colors} = useTheme();
  const {text1} = state; // with its type we can handle different types of toast
  return (
    <View style={styles.mainContainer}>
      <View
        style={[
          styles.container,
          {
            borderColor: colors.primary,
            backgroundColor: colors.background,
            shadowColor: colors.shadowColor,
          },
        ]}>
        <Image
          source={Icon.info}
          style={[styles.imageStyle, {tintColor: colors.primary}]}
        />
        <Text
          numberOfLines={2}
          style={[styles.textStyle, {color: colors.text}]}>
          {text1}
        </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    width: '100%',
    height: 60,
    bottom: 20,
  },
  container: {
    flex: 1,
    flexDirection: 'row',
    marginHorizontal: 20,
    alignItems: 'center',
    backgroundColor: 'red',
    borderLeftWidth: 8,
    borderRadius: 8,
    shadowOffset: {width: 0, height: 0},
    shadowOpacity: 0.4,
    shadowRadius: 2,
    elevation: 8,
  },
  textStyle: {
    fontSize: 14,
    fontWeight: 'bold',
    flex: 1,
    marginRight: 10,
  },
  imageStyle: {
    width: 25,
    height: 25,
    marginHorizontal: 15,
  },
});
